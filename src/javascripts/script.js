/* Author:

*/


// Global Variables

var isMobile = /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent);



// Miscellaneous JS

(function($) {

	'use strict';

	function navbarMenu() {
		$('.navbar-menu').on('click', function() {
			$('body').addClass('menu-open');
			$('.mobile-backdrop').fadeIn();
			$('html, body').animate({
				scrollTop: $('.main').offset().top
			}, 300, 'easeInOutExpo');
		});

		$('.navbar-menu-close').on('click', function() {
			$('body').removeClass('menu-open');
			$('.mobile-backdrop').fadeOut();
		});
	}




	function resizeInit() {
		$(window).on('resize', function() {
			var windowY = $(window).height();
			$('.navbar-menu-content').height(windowY);

		});

		$(window).trigger('resize');
	}


	// Custom Select

		$('.custom-select').each(function() {
			var $this = $(this),
				$option = $this.children('option'),
				$optionLength = $option.length;

			$this.hide();
			$this.wrap('<div class="dd-select"></div>');
			$this.after('<div class="dd-select-text"></div>');


			
			var $selectText = $this.next('.dd-select-text');
			$selectText.text($option.eq(0).text());

			var $ul = $('<ul />', {
				'class': 'dd-list'
			}).insertAfter($selectText);	

			for (var i = 0; i < $optionLength; i++) {
				$('<li />', {
					text: $option.eq(i).text(),
					rel: $option.eq(i).val()
				}).appendTo($ul);
			}


			var $listItems = $ul.children('li');
			var $ddList = $this.find('.dd-list');

			$selectText.on('click', function(event) {
				event.preventDefault();
				event.stopPropagation();
				$('.dd-select-text').removeAttr('style');
				$('.dd-list').slideUp(250);
				$('.dd-select-text').removeClass('active');
				if ($ul.is(':hidden')) {
					$(this).addClass('active');
					$selectText.css({
						'text-indent': -99999
					})
					$ul.slideDown(250);
				} else {
					$(this).removeClass('active');
					$ul.slideUp(250);
					$selectText.removeAttr('style');
				}
			});


			$listItems.on('click', function(event) {
				event.preventDefault();
				event.stopPropagation();
				var $thisValue = $(this).text();
				$selectText.text($thisValue);
				$selectText.removeClass('active');
				$ul.slideUp(250);
				$selectText.removeAttr('style');
			});


			$(document).on('click', function(event) {
				$ul.slideUp(250);
				$selectText.removeClass('active');
				$selectText.removeAttr('style');
			});

		});



	$(function() {
		FastClick.attach(document.body);

		resizeInit();
		navbarMenu();





	});
		
})(jQuery);