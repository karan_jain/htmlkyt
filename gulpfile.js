// Require gulp
var gulp = require('gulp');

// Require all plugins
var sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	prefix = require('gulp-autoprefixer'),
	browserSync = require('browser-sync'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify');
	pug = require('gulp-pug');




// Config

var config = {
	bowerDir: 'bower_components/'
};



// ALL Javascripts

var allScripts = [

	// Vendor Plugins
	config.bowerDir + 'jQuery/dist/jquery.js',
	config.bowerDir + 'bootstrap-sass/assets/javascripts/bootstrap.min.js',
	config.bowerDir + 'fastclick/lib/fastclick.js',
	config.bowerDir + 'magnific-popup/dist/jquery.magnific-popup.js',
	config.bowerDir + 'slick-carousel/slick/slick.js',
	
	
	// Custom Script
	'src/javascripts/vendor/*.js',
	'src/javascripts/**/*.js'
];

var allStyles = [

	// Vendor Plugins
	config.bowerDir + 'magnific-popup/dist/magnific-popup.css',
	config.bowerDir + 'slick-carousel/slick/slick.css',
	config.bowerDir + 'slick-carousel/slick/slick-theme.css'
];


// Using gulp-sass
gulp.task('sass', function(){
	return gulp.src('src/sass/**/*.scss')
  		.pipe(sourcemaps.init())
    	.pipe(sass({outputStyle: 'compressed'})) 
    	.pipe(prefix({
    		browsers: ['last 2 versions', 'ie >= 9']
    	}))
    	.pipe(sourcemaps.write('/../maps'))
    	.pipe(gulp.dest('src/build/css'))
    	.pipe(browserSync.stream());
});

// Using gulp-sass
gulp.task('bowercss', function(){
	return gulp.src(allStyles)
    	.pipe(concat('bower.css'))
  		.pipe(gulp.dest('src/build/css'))
    	.pipe(browserSync.stream());
});

gulp.task('pug-html', function buildHTML() {
  return gulp.src('src/pug/pages/*.pug')
  .pipe(pug({pretty: true}))
  .pipe(gulp.dest('src/html'))
});



// Watch Task
gulp.task('watch', ['sass', 'pug-html'], function() {
	gulp.watch('src/sass/**/*.scss', ['sass']);
	gulp.watch('src/javascripts/**/*.js', ['scripts']);
	gulp.watch('src/pug/*.pug', ['pug-html']);
});


// Uglify Task
gulp.task('scripts', function() {
	return gulp.src(allScripts)
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(gulp.dest('src/build/javascripts/'));
});




// Browser-sync
gulp.task('browserSync', function() {
	browserSync({
		server: {
			baseDir: 'src'
		}
	});
});



// Default Task
gulp.task('default', ['watch', 'sass', 'pug-html', 'bowercss', 'scripts']);